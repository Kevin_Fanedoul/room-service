import fr.iut.rm.persistence.domain.Room;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by jkfanedoul on 18/02/16.
 */

/**
 * A classic room
 */
@Entity
@Table(name = "AccessEvent")
public class AccessEvent {

    /**
     * sequence generated id
     */
    @Id
    @GeneratedValue
    private long id;

    /**
     * First name
     */
    @Column(nullable = false)
    private String firstName="";

    /**
     * Dates
     */
    @Column(nullable = false)
    private Date dateEntree;
    @Column(nullable = false)
    private Date dateSortie;

    @Column(nullable = false)
    private TypeOfEvent eventType;

    @Column(nullable = false)
    private Room myRoom;

    public AccessEvent(){

    }












    /**
     * SETTER & GETTER
     * @return PARAMS
     */

    public TypeOfEvent getEventType() {
        return eventType;
    }

    public void setEventType(TypeOfEvent eventType) {
        this.eventType = eventType;
    }

    public Date getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }

    public Date getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(Date dateEntree) {
        this.dateEntree = dateEntree;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Room getMyRoom() {
        return myRoom;
    }

    public void setMyRoom(Room myRoom) {
        this.myRoom = myRoom;
    }
}

